import java.util.Random;

/**
 * Created by amen on 8/21/17.
 */

public class RandomStrategy implements IInputStrategy {

    Random random = new Random();
    private final String ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_";


    public RandomStrategy() {
    }

    @Override
    public String getString() {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            string.append(ALPHABET.charAt(random.nextInt(ALPHABET.length())));
        }
        return string.toString();

    }

    @Override
    public double getDouble() {
        return random.nextDouble();
    }

    @Override
    public int getInt() {
        return random.nextInt();
    }
}
