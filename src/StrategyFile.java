import java.io.*;
import java.util.Scanner;

public class StrategyFile implements IInputStrategy {

    public StrategyFile() {
    }

    //    Scanner scanner = new Scanner(System.in);
//    File file = new File("plik.txt");
   // Scanner scanner = new Scanner(new FileReader("plik.txt"));


    @Override
    public int getInt() {
        int a = 0;
        try (Scanner scanner = new Scanner(new FileReader("plikInt.txt"))) {
                a = scanner.nextInt();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return a;
    }

    @Override
    public String getString() {
        String s = null;
        try (Scanner scanner = new Scanner(new FileReader("plikString.txt"))) {
            s = scanner.nextLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return s;

    }

    @Override
    public double getDouble() {
        double a = 0.0;
        try (Scanner scanner = new Scanner(new FileReader("plikDouble.txt"))) {
                a = scanner.nextDouble();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return a;
    }
}
